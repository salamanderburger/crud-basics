﻿--------------------------------------------------------
--  File created - Friday-October-05-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table STUDENTSXCLASSES
--------------------------------------------------------

  CREATE TABLE "STUDENTSXCLASSES" 
   (	"STUDENTXCLASSID" BIGINT IDENTITY(1,1) PRIMARY KEY, 
	"STUDENTID" BIGINT, 
	"CLASSID" BIGINT
   );
-- INSERTING into STUDENTSXCLASSES
/* SET DEFINE OFF; */

﻿--------------------------------------------------------
--  File created - Friday-October-05-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CLASSES
--------------------------------------------------------

  CREATE TABLE "CLASSES" 
   (	"CLASSID" BIGINT IDENTITY(1,1) PRIMARY KEY, 
	"CLASSCODE" VARCHAR(255), 
	"TEACHERID" BIGINT, 
	"STARTDATE" DATETIME, 
	"FINISHDATE" DATETIME, 
	"CLASSNAME" VARCHAR(255)
   );
-- INSERTING into CLASSES
/* SET DEFINE OFF; */
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5101',1,convert(DATETIME, '04-SEP-18',3),convert(DATETIME, '14-DEC-18',3),'Web Application Development');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5102',2,convert(DATETIME, '04-SEP-18',3),convert(DATETIME, '14-DEC-18',3),'Project Management');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5103',5,convert(DATETIME, '04-SEP-18',3),convert(DATETIME, '14-DEC-18',3),'Web Programming');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5104',7,convert(DATETIME, '04-SEP-18',3),convert(DATETIME, '14-DEC-18',3),'Digital Design');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5105',8,convert(DATETIME, '04-SEP-18',3),convert(DATETIME, '14-DEC-18',3),'Database Development');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5201',2,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'Security & Quality Assurance');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5202',3,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'Web Application Development 2');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5203',4,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'XML and Web Services');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5204',5,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'Mobile Development');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5205',6,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'Career Connections');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('http5206',8,convert(DATETIME, '08-01-19',3),convert(DATETIME, '27-04-19',3),'Web Information Architecture');
Insert into CLASSES (CLASSCODE,TEACHERID,STARTDATE,FINISHDATE,CLASSNAME) values ('PHYS2203',10,convert(DATETIME, '04-SEP-19',3),convert(DATETIME, '14-DEC-19',3),'Massage Therapy');

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class Class : System.Web.UI.Page
    {
        //export this to be property of the entire page
        public string classid {
            get { return Request.QueryString["classid"]; }
        }
        private string studentsxclasses_basequery =
             "SELECT students.studentid," +
            " concat(studentlname, ' ' ,studentfname) as 'Students Enrolled', studentnumber as Number " +
            " from classes " +
            " left join studentsxclasses on classes.classid= studentsxclasses.classid" +
            " left join students on students.studentid = studentsxclasses.studentid ";
        private string class_basequery =
            "SELECT teachers.teacherid, classname as 'Class Name', classcode as Code, concat(teacherfname,' ',teacherlname) as 'Teacher', " +
            " convert(varchar, startdate,106) as 'Start Date'," +
            " convert(varchar, finishdate,106) as 'Finish Date'" +
            " from classes " +
            "left join teachers on teachers.teacherid = classes.teacherid";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (classid == "" || classid == null) class_name.InnerHtml = "No class found.";
            else
            {
                //Need to use custom rendering on this one to get the class link
                class_basequery += " WHERE CLASSID = " + classid;
                //debug.InnerHtml = student_basequery;
                class_select.SelectCommand = class_basequery;
                class_query.InnerHtml = class_basequery;

                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView classview = (DataView)class_select.Select(DataSourceSelectArguments.Empty);
                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView studentrowview = classview[0]; //gets the first result which is always the student
                string classname = classview[0]["Class Name"].ToString();
                class_name.InnerHtml = classname;

                class_list.DataSource = Class_Manual_Bind(class_select);
                class_list.DataBind();

                studentsxclasses_basequery += " WHERE CLASSES.CLASSID=" + classid;
                studentsxclasses_basequery += " order by studentlname desc," +
                    " studentfname desc "; //This is not part of the "base query"
                //debug.InnerHtml = studentsxclasses_basequery;
                studentxclass_select.SelectCommand = studentsxclasses_basequery;
                studentxclass_query.InnerHtml = studentsxclasses_basequery;
                //studentxclass_list.DataSource = studentxclass_select;
                studentxclass_list.DataSource = StudentxClass_Manual_Bind(studentxclass_select);
                studentxclass_list.DataBind();

            }
        }
        protected DataView Class_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Teacher"] =
                    "<a href=\"Teacher.aspx?teacherid="
                    + row["teacherid"]
                    + "\">"
                    + row["Teacher"]
                    + "</a>";

            }
            mytbl.Columns.Remove("teacherid");
            myview = mytbl.DefaultView;

            return myview;
        }


        //We need a manual bind for the class list to provide a link to the class
        protected DataView StudentxClass_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Students Enrolled"] =
                    "<a href=\"Student.aspx?studentid="
                    + row["studentid"]
                    + "\">"
                    + row["Students Enrolled"]
                    + "</a>";

            }
            mytbl.Columns.Remove("studentid");
            myview = mytbl.DefaultView;

            return myview;

        }

        protected void DelClass(object sender, EventArgs e)
        {
            //Don't want to use a base string for delete 
            //because "DELETE FROM CLASSES"
            //should never be executed, it's very dangerous to delete!
            string delquery = "DELETE FROM CLASSES WHERE classid="+classid;

            del_debug.InnerHtml = delquery;
            del_class.DeleteCommand = delquery;
            del_class.Delete();

            //When a class is deleted it can't have students
            //This can either be handled by a trigger (DB decision)
            //or within the delete function (Server decision).
            //This is an example of a server decision.
            string delstudentsxclassesquery = "DELETE FROM" +
                " STUDENTSXCLASSES WHERE CLASSID=" + classid;
            del_debug.InnerHtml += "<br>BUT ALSO<br>" + delstudentsxclassesquery;
            del_class.DeleteCommand = delstudentsxclassesquery;
            del_class.Delete();
        }
    }
}
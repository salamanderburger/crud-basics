﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace http5101_crud_basics
{
    public partial class EditClass : System.Web.UI.Page
    {

        //export this to be property of the entire page
        public int classid
        {
            get { return Convert.ToInt32(Request.QueryString["classid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Edit_Class()
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView studentrow = getClassInfo(classid);
            if (studentrow == null)
            {
                class_fullname.InnerHtml = "No Class Found.";
                return;
            }
            class_name.Text = studentrow["classname"].ToString();
            //debug.InnerHtml = studentrow["classcode"].ToString().Substring(0, 4).ToUpper();
            class_code.SelectedValue = studentrow["classcode"].ToString().Substring(0,4).ToUpper();
            //put this into the title so we know who we're editing
            class_number.Text = studentrow["classcode"].ToString().Substring(4);

        }

        protected void Edit_Class(object sender, EventArgs e)
        {
            //change this
            string name = class_name.Text;
            string code = class_code.SelectedValue;
            string number = class_number.Text;
            /*
            string startdate = student_section.SelectedValue.ToString();
            string enddate = 
            */
            

            string editquery = "Update classes set classname='"+name+"'," +
                " classcode='" +code + number + "'"+
                "where classid="+classid;
            debug.InnerHtml = editquery;
            
            edit_class.UpdateCommand = editquery;
            edit_class.Update();
            

        }

        protected DataRowView getClassInfo(int id)
        {
            string query = "select * from classes where classid=" + classid.ToString();
            class_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the students name outside of
            //the datagrid.
            DataView classview = (DataView)class_select.Select(DataSourceSelectArguments.Empty);

            //If there is no student in the studentview, an invalid id is passed
            if (classview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView classrowview = classview[0]; //gets the first result which is always the student
            //string studentinfo = studentview[0][colname].ToString();
            return classrowview;

        }
    }
}
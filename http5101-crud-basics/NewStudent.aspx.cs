﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class NewStudent : System.Web.UI.Page
    {
        private string addquery = "INSERT into students " +
            "(studentfname, studentlname, studentnumber, studentsection, enrolmentdate)" +
            " VALUES ";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddStudent(object sender, EventArgs e)
        {
            string fname = student_fname.Text;
            string lname = student_lname.Text;
            string num = student_number.Text;
            string section = student_section.SelectedValue;
            //This is a little silly but we're getting the current date
            //from a date time > putting it into a string and
            //converting it back into a datetime in our insert statement.
            string now = DateTime.Today.ToString("dd/MM/yyyy");

            addquery += "('"+fname+"', '"+lname+"', '"+
                num+ "','"+section+"',convert(DATETIME,'" + now+"',103))";

            insert_student.InsertCommand = addquery;
            insert_student.Insert();
            debug.InnerHtml = addquery;

        }
    }
}
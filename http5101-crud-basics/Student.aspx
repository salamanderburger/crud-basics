﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="http5101_crud_basics.Student" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 id="student_name" runat="server"></h3>

    <%-- data source object which shows base info about the student --%>
   <asp:SqlDataSource runat="server"
        id="student_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- data source object which shows the classes a student belongs to --%>
    <asp:SqlDataSource runat="server"
        id="studentxclass_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- container for representing base info  --%>
    <div id="student_query" runat="server" class="querybox">

    </div>
    <asp:DataGrid ID="student_list" runat="server">

    </asp:DataGrid>
    <%-- container for representing class info --%>
    <div id="studentxclass_query" runat="server" class="querybox">

    </div>
    <asp:DataGrid ID="studentxclass_list" runat="server">

    </asp:DataGrid>
    <div runat="server" id="_addstudentxclass">
        <h4>Add this student to a class!</h4>
        <asp:SqlDataSource runat="server"
            id="insert_studentxclass"
            ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

        </asp:SqlDataSource>
        <div class="inputrow">
            <ASP:ClassPick id="student_class" runat="server">
            </ASP:ClassPick>
        </div>
        <div class="inputrow">
            <asp:Button runat="server" Text="Add Class" OnClick="Add_StudentxClass"/>
        </div>
        <div id="debug_addclass" class="querybox" runat="server">

        </div>
    </div>
</asp:Content>

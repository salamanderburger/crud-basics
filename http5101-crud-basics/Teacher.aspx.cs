﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class Teacher : System.Web.UI.Page
    {
        private string teacher_basequery = "SELECT CONCAT(teacherfname, ' ', teacherlname) as Name, Employeenumber as Number, convert(varchar, hiredate,106) as Hired, '$'+convert(varchar, salary) as Salary FROM TEACHERS";

        private string teacher_class_basequery =
            "SELECT classes.classid, classcode+' - '+classname as 'Teaching Semester' from teachers " +
            " left join classes on teachers.teacherid = classes.teacherid ";

        protected void Page_Load(object sender, EventArgs e)
        {
            string teacherid = Request.QueryString["teacherid"];
            if (teacherid == ""||teacherid ==null) teacher_name.InnerHtml = "No teacher found.";
            else
            {
                //We can use the default data bind here
                //nothing special we need to do to manipulate the rendering
                teacher_basequery += " WHERE TEACHERID = " + teacherid;
                //debug.InnerHtml = student_basequery;
                teacher_select.SelectCommand = teacher_basequery;

                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView teacherview = (DataView)teacher_select.Select(DataSourceSelectArguments.Empty);
                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView teacherrowview = teacherview[0]; //gets the first result which is always the student
                string teachername = teacherview[0]["Name"].ToString();
                teacher_name.InnerHtml = teachername;

                teacher_query.InnerHtml = teacher_basequery;

                teacher_list.DataSource = teacher_select;
                teacher_list.DataBind();

                teacher_class_basequery += " WHERE TEACHERS.TEACHERID=" + teacherid;
                //debug.InnerHtml = studentsxclasses_basequery;
                teacher_class_select.SelectCommand = teacher_class_basequery;

                teacher_class_query.InnerHtml = teacher_class_basequery;

                teacher_class_list.DataSource = Teacher_Manual_Bind(teacher_class_select);
                teacher_class_list.DataBind();

            }
        }

        //We need a manual bind for the class list to provide a link to the class
        protected DataView Teacher_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Teaching Semester"] =
                    "<a href=\"Class.aspx?classid="
                    + row["classid"]
                    + "\">"
                    + row["Teaching Semester"]
                    + "</a>";

            }
            mytbl.Columns.Remove("classid");
            myview = mytbl.DefaultView;

            return myview;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics.usercontrols
{
    public partial class ClassPick : System.Web.UI.UserControl
    {
        //base query
        private string basequery = "SELECT classid as id," +
            "CONCAT(classcode, ' - ',classname) as class from classes";

        private int selected_id; //Actual field
        public int _selected_id // Accessor
        {
            //standard property accessors
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //We don't need to add onto the base query.
            //However, if we were imagining a more advanced app,
            //We could imagine showing classes that the particular
            //student is not yet enrolled in!
            //The narrative builds the code!

            classes_list_pick.SelectCommand = basequery;
            //Did you think that datagrids were the only
            //Thing you could bind to an element?
            //However, we need to render it manually (again).

            //Instead of converting a datasource we instead
            //Manually add items onto the list.
            ClassPick_Manual_Bind(classes_list_pick, "class_pick");

        }

        //take an id (of a dropdownlist) and a datasource.
        //iterate through the datasource and add list items
        //to the dropdownlist.
        void ClassPick_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            //The rendering loop here is to print out list items
            //For our asp drop down list.


            DropDownList classlist = (DropDownList)FindControl(ddl_id);
            //classlist.Items.Clear();

            //Data source > View > Table
            //Don't need to modify the datatable because we're not
            //directly binding the source to the dropdownlist,
            //instead we're reading the data then applying it to a 
            //dropdownlist we create manually.
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                //Intercept data rendering from DB
                //make some list items for our dropdownlist
                ListItem class_item = new ListItem();
                class_item.Text = row["class"].ToString();
                class_item.Value = row["id"].ToString();
                classlist.Items.Add(class_item);
            }
        }

        protected void Assign_ClassID(object sender, EventArgs e)
        {
            //Whenever the dropdownlist is changed,
            //set the TeacherPick field "selected_id" to the value.
            //We do this just so we can grab this value in any page 
            //which calls the teacher pick user control
            _selected_id = int.Parse(class_pick.SelectedValue);
        }
    }
}

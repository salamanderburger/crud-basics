﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class TeacherPick : System.Web.UI.UserControl
    {
        //base query
        private string basequery = "SELECT teacherid as id," +
            "CONCAT(teacherfname, ' ',teacherlname) as teacher from teachers";

        private int selected_id; //Actual field
        public int _selected_id // Accessor
        {
            //standard property accessors
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //We don't need to add onto the base query.
            //However, if we were imagining a more advanced app,
            //We could think about only picking teachers that are
            //teaching under the "maximum workload" 
            //(5 classes in one semester)
            //The narrative builds the code!

            teachers_list_pick.SelectCommand = basequery;
            //Did you think that datagrids were the only
            //Thing you could bind to an element?
            //However, we need to render it manually (again).

            //Instead of converting a datasource we instead
            //Manually add items onto the list.
            TeacherPick_Manual_Bind(teachers_list_pick, "teacher_pick");
            
        }

        //take an id (of a dropdownlist) and a datasource.
        //iterate through the datasource and add list items
        //to the dropdownlist.
        void TeacherPick_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            //The rendering loop here is to print out list items
            //For our asp drop down list.


            DropDownList teacherlist = (DropDownList)FindControl(ddl_id);
            //If you don't include this silly things happen
            //teacherlist.Items.Clear();

            //Data source > View > Table
            //Don't need to modify the datatable because we're not
            //directly binding the source to the dropdownlist,
            //instead we're reading the data then applying it to a 
            //dropdownlist we create manually.
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                //Intercept data rendering from DB
                //make some list items for our dropdownlist
                ListItem teacher_item = new ListItem();
                teacher_item.Text = row["teacher"].ToString();
                teacher_item.Value = row["id"].ToString();
                teacherlist.Items.Add(teacher_item);
            }
        }

        protected void Assign_TeacherID(object sender, EventArgs e)
        {
            //Whenever the dropdownlist is changed,
            //set the TeacherPick field "selected_id" to the value.
            //We do this just so we can grab this value in any page 
            //which calls the teacher pick user control
            _selected_id = int.Parse(teacher_pick.SelectedValue);
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassPick.ascx.cs" Inherits="http5101_crud_basics.usercontrols.ClassPick" %>
<label>Pick Class</label>
<%--
    We want an interface which populates a list of current classes
    for our user to select from.
--%>
<asp:SqlDataSource 
    runat="server" 
    ID="classes_list_pick"
    ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
</asp:SqlDataSource>
<asp:DropDownList 
    runat="server" 
    ID="class_pick"
    OnTextChanged="Assign_ClassID">

</asp:DropDownList>
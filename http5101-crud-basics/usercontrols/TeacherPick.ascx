﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TeacherPick.ascx.cs" Inherits="http5101_crud_basics.TeacherPick" %>
<label>Pick Teacher</label>
<%--
    We want an interface which populates a list of current teachers
    for our user to select from.

    Be careful with your IDs, teachers_list is used in teachers.aspx.
    although we are trying to communicate the same idea 
    (a list of teachers).
--%>
<asp:SqlDataSource 
    runat="server" 
    ID="teachers_list_pick"
    ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
</asp:SqlDataSource>
<asp:DropDownList 
    runat="server" 
    ID="teacher_pick"
    OnTextChanged="Assign_TeacherID">

</asp:DropDownList>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class Student : System.Web.UI.Page
    {
        private string student_basequery = "SELECT concat(studentfname, ' ', studentlname) as Name, studentnumber as Number, convert(varchar,enrolmentdate,106) as Enrolled, studentsection as Section FROM STUDENTS ";

        private string studentid
        { 
            get {return Request.QueryString["studentid"];}
        }

        private string studentsxclasses_basequery =
            "SELECT classes.classid, classcode+' - '+classname as 'Enrolled Courses' from classes" +
            " left join studentsxclasses on classes.classid = studentsxclasses.classid" + 
            " left join students on students.studentid = studentsxclasses.studentid ";

        protected void Page_Load(object sender, EventArgs e)
        {
            //make the "add this student to class" div invisible, if there is a
            //student, make it visible. 
            _addstudentxclass.Visible = false;
            if (studentid == "" || studentid == null)
            {
                student_name.InnerHtml = "No student found.";
                return;
            }
            else
            {
                //We can use the default data bind here
                //nothing special we need to do to manipulate the rendering
                student_basequery += " WHERE STUDENTID = " + studentid;
                //debug.InnerHtml = student_basequery;
                student_select.SelectCommand = student_basequery;
                student_query.InnerHtml = student_basequery;
                //debug.InnerHtml = student_basequery;


                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView studentview = (DataView)student_select.Select(DataSourceSelectArguments.Empty);

                //If there is no student in the studentview, an invalid id is passed
                if (studentview.ToTable().Rows.Count < 1)
                {
                    student_name.InnerHtml = "No student found.";
                    return;
                }
                _addstudentxclass.Visible = true;

                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView studentrowview = studentview[0]; //gets the first result which is always the student
                string studentname = studentview[0]["Name"].ToString();
                student_name.InnerHtml = studentname;

                student_list.DataSource = student_select;
                student_list.DataBind();

                studentsxclasses_basequery += " WHERE STUDENTS.STUDENTID=" + studentid;
                //debug.InnerHtml = studentsxclasses_basequery;
                studentxclass_select.SelectCommand = studentsxclasses_basequery;
                studentxclass_query.InnerHtml = studentsxclasses_basequery;
                studentxclass_list.DataSource = StudentxClass_Manual_Bind(studentxclass_select);
                studentxclass_list.DataBind();

            }
        }

        //We need a manual bind for the class list to provide a link to the class
        protected DataView StudentxClass_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Enrolled Courses"] =
                    "<a href=\"Class.aspx?classid="
                    + row["classid"]
                    + "\">"
                    + row["Enrolled Courses"]
                    + "</a>";

            }
            mytbl.Columns.Remove("classid");
            myview = mytbl.DefaultView;

            return myview;

        }

        protected void Add_StudentxClass(object sender, EventArgs e)
        {
            string addstudentxclassquery = "insert into studentsxclasses ( " +
                "studentid, classid) values ("; 
            
            //This line of code pulls a field from the 
            //ClassPick user control selected value
            int classid = student_class._selected_id;

            addstudentxclassquery += studentid + "," + classid + ")";

            debug_addclass.InnerHtml = addstudentxclassquery;

            insert_studentxclass.InsertCommand = addstudentxclassquery;
            insert_studentxclass.Insert();

            //refresh the look of the classes that the student's in
            studentxclass_list.DataSource = StudentxClass_Manual_Bind(studentxclass_select);
            studentxclass_list.DataBind();
        }
    }
}
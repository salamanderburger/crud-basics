﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace http5101_crud_basics
{
    public partial class Teachers : Page
    {
        private string sqlline = "SELECT teacherid, CONCAT(teacherfname, ' ', teacherlname) as Name, Employeenumber as Number, convert(varchar,hiredate,106) as Hired, '$'+convert(varchar,salary) as Salary FROM TEACHERS";

        protected void Page_Load(object sender, EventArgs e)
        {
            teachers_select.SelectCommand = sqlline;
            teacher_query.InnerHtml = sqlline;
            teachers_list.DataSource = Teachers_Manual_Bind(teachers_select);
            teachers_list.DataBind();

        }

        //We need a manual bind to provide link to the teacher
        protected DataView Teachers_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Name"] =
                    "<a href=\"Teacher.aspx?teacherid="
                    + row["teacherid"]
                    + "\">"
                    + row["Name"]
                    + "</a>";

            }
            mytbl.Columns.Remove("teacherid");
            myview = mytbl.DefaultView;

            return myview;

        }
    }
}
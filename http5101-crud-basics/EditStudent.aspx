﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditStudent.aspx.cs" Inherits="http5101_crud_basics.EditStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- One SQL command for viewing the existing info --%>
    <asp:SqlDataSource runat="server" id="student_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>

    <%-- One SQL command for editing the student --%>
    <asp:SqlDataSource runat="server" id="edit_student"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>

    <h3 runat="server" id="student_name">Update </h3>

    <div class="inputrow">
        <label>First Name:</label>
        <asp:textbox id="student_fname" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Last Name:</label>
        <asp:textbox id="student_lname" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Number:</label>
        <asp:textbox id="student_number" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Section:</label>
        <asp:radiobuttonlist id="student_section" runat="server">
            <asp:listitem runat="server" text="A" value="A"></asp:listitem>
            <asp:listitem runat="server" text="B" value="B"></asp:listitem>
        </asp:radiobuttonlist>
    </div>
     
    <asp:Button Text="Edit Student" runat="server" OnClick="Edit_Student"/>
    
    <div class="querybox" id="debug" runat="server">

    </div>
    
</asp:Content>

﻿<%@ Page Title="Classes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="http5101_crud_basics.Classes" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <!-- Create a page which describes a new class -->
    <a href="NewClass.aspx">New Class</a>
    <%--
    Microsoft SQL uses + for string concatenation instead
    of Oracle SQL's ||.
    --%>
    <asp:SqlDataSource runat="server"
        id="classes_select"
       
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <div id="classes_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="classes_list"
        runat="server" >
    </asp:DataGrid>

    <div id="debug" runat="server"></div>
</asp:Content>

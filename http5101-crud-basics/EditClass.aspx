﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditClass.aspx.cs" Inherits="http5101_crud_basics.EditClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h3 runat="server" id="class_fullname">Edit Class</h3>
    <asp:SqlDataSource runat="server" id="edit_class"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="class_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <div class="inputrow">
        <label>Class Name:</label>
        <asp:TextBox ID="class_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="class_name"
            ErrorMessage="Enter a class name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
    <label>Program</label>
    
    <asp:DropDownList ID="class_code" runat="server">
        <asp:ListItem value="HTTP" Text="HTTP Web Development"></asp:ListItem>
        <asp:ListItem value="ACCT" Text="ACCT Accounting"></asp:ListItem>
        <asp:ListItem value="PHYS" Text="PHYS Physical Therapy"></asp:ListItem>
        <asp:ListItem value="ANIM" Text="ANIM 3D Modelling & Animation"></asp:ListItem>
        <asp:ListItem value="CRIM" Text="CRIM Criminal Justice"></asp:ListItem>
    </asp:DropDownList>
    </div>
    
    <div class="inputrow">
        <label>Number:</label>
        <asp:TextBox runat="server" ID="class_number"></asp:TextBox>
        <asp:RequiredFieldValidator ID="class_number_validate" controlToValidate="class_number"></asp:RequiredFieldValidator>
    </div>
    <%--
    <div class="inputrow">
        <label>Semester</label>
        
        <asp:RadioButtonList id="class_semester" runat="server">
            <asp:ListItem value="F_2018" Text="Fall 2018"></asp:ListItem>
            <asp:ListItem value="W_2019" Text="Winter 2019"></asp:ListItem>
            <asp:ListItem value="S_2019" Text="Summer 2019"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div class="inputrow">
        <ASP:TeacherPick ID="class_teacher" runat="server">

        </ASP:TeacherPick>
    </div>
--%>
    <ASP:Button Text="Edit Class" runat="server" OnClick="Edit_Class"/>

    <div runat="server" id="debug" class="querybox"></div>
    
</asp:Content>

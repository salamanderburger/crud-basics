﻿<%@ Page Title="Students" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="http5101_crud_basics.Students" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <!-- interface for adding new students -->
    <div id="inputrow">
        <a href="NewStudent.aspx">New Student</a>
    </div>
    <!-- Interface for searching student records-->
    <div id="sections">
        <asp:CheckBoxList runat="server" ID="student_sec">
            <asp:ListItem Value="A" Text="Section A"></asp:ListItem>
            <asp:ListItem Value="B" Text="Section B"></asp:ListItem>
        </asp:CheckBoxList>
    </div>
    <asp:TextBox runat="server" ID="student_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Students"/>
   
    <!-- Result window for showing the generated query -->
    <div id="student_query" class="querybox" runat="server">
    </div>
    <div id="debug" runat="server">
    </div>
    <%-- 
        Connection string for built in SQL system
    --%>
    <asp:SqlDataSource runat="server"
        id="students_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <asp:DataGrid id="students_list"
        runat="server" AutoGenerateColumns="False"
        OnItemCommand="Students_ItemCommand">
    </asp:DataGrid>

    <%-- 
     Data Source for deleting students and studentsxclasses   
        --%>
    <asp:SqlDataSource runat="server"
        id="del_student"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server"
        id="del_studentxclass"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>

</asp:Content>

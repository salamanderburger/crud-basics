﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class NewClass : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO CLASSES" +
            "(classcode,classname," +
            "teacherid,startdate,finishdate) VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddClass(object sender, EventArgs e)
        {
            //first get the inputs
            string name = class_name.Text.ToString();
            string code = class_code.SelectedValue+class_number.Text;
            string semester = class_semester.SelectedValue;
            //teacher_pick is the DDL inside the usercontrol,
            //It's more difficult to get the value.

            //instead we can use a field on the class of
            //TeacherPick (the control), to pass along the selected ID
            int teacherid = class_teacher._selected_id;
            string s_d = "";
            string e_d = "";

            switch (semester[0])
            {
                //This is now a char, not a string
                case 'F':
                    s_d = "04/09/";
                    e_d = "14/12/";
                    break;
                case 'W':
                    s_d = "08/01/";
                    e_d = "27/04/";
                    break;
                case 'S':
                    s_d = "06/05/";
                    e_d = "16/08/";
                    break;
                
            }
            s_d = "convert(DATETIME,'"+s_d+semester.Substring(2)+"',103)"; //adds year
            e_d = "convert(DATETIME,'"+e_d+semester.Substring(2) + "',103)"; //adds year


            //We implemented a sequence by setting
            //the identity in the primary key of the import scripts
            //redownload the import scripts to get it working.
            addquery += "('" +
                code+"','"+name+"',"+teacherid+","+s_d+","+e_d+")";

            debug.InnerHtml = addquery;

            //Finally when we've done all our trickery to make
            //the sql command work, we can now do it!
            insert_class.InsertCommand = addquery;
            insert_class.Insert();
        }
    }
}
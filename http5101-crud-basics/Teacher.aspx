﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Teacher.aspx.cs" Inherits="http5101_crud_basics.Teacher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 id="teacher_name" runat="server"></h3>
    <%-- data source object which shows base info about the teacher --%>
   <asp:SqlDataSource runat="server"
        id="teacher_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- data source object which shows the classes that teacher teaches --%>
    <asp:SqlDataSource runat="server"
        id="teacher_class_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- container for representing base info  --%>
    <div id="teacher_query" runat="server" class="querybox">

    </div>
    <asp:DataGrid ID="teacher_list" runat="server">

    </asp:DataGrid>
    <%-- container for representing class info --%>
    <div id="teacher_class_query" runat="server" class="querybox">

    </div>
    <asp:DataGrid ID="teacher_class_list" runat="server">

    </asp:DataGrid>




</asp:Content>

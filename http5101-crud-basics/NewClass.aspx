﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewClass.aspx.cs" Inherits="http5101_crud_basics.NewClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h3>New Class</h3>
    <asp:SqlDataSource runat="server" id="insert_class"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource>
    <div class="inputrow">
        <label>Class Name:</label>
        <asp:TextBox ID="class_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="class_name"
            ErrorMessage="Enter a class name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
    <label>Program</label>
    <%--
        Sometimes it's worth revisiting the database architecture
        When more data provides us with a context. For example,
        It would make a lot more sense to have another table "Programs"
        and a "Program Code", with each class linking to a programid.
    --%>
    <asp:DropDownList ID="class_code" runat="server">
        <asp:ListItem value="HTTP" Text="HTTP Web Development"></asp:ListItem>
        <asp:ListItem value="ACCT" Text="ACCT Accounting"></asp:ListItem>
        <asp:ListItem value="PHYS" Text="PHYS Physical Therapy"></asp:ListItem>
        <asp:ListItem value="ANIM" Text="ANIM 3D Modelling & Animation"></asp:ListItem>
        <asp:ListItem value="CRIM" Text="CRIM Criminal Justice"></asp:ListItem>
    </asp:DropDownList>
    </div>
    <div class="inputrow">
        <label>Number:</label>
        <asp:TextBox runat="server" ID="class_number"></asp:TextBox>
        <asp:RequiredFieldValidator ID="class_number_validate" controlToValidate="class_number"></asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <label>Semester</label>
        <%--
            What problems arise from "hard coding" 2018 and 2019?
            Notice how we prefer this interface over "StartDate" and "EndDate"
            calendar pickers, but we could have used that too.
        --%>
        <asp:RadioButtonList id="class_semester" runat="server">
            <asp:ListItem value="F_2018" Text="Fall 2018"></asp:ListItem>
            <asp:ListItem value="W_2019" Text="Winter 2019"></asp:ListItem>
            <asp:ListItem value="S_2019" Text="Summer 2019"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
<%--
    What if we want to add a teacher to this class? We certainly
    can't hard code the teachers. Maybe if we had a user control which 
    creates a dropdownlist of the teachers?
--%>
    <div class="inputrow">
        <ASP:TeacherPick ID="class_teacher" runat="server">

        </ASP:TeacherPick>
    </div>

    <ASP:Button Text="Add Class" runat="server" OnClick="AddClass"/>

    <div runat="server" id="debug" class="querybox"></div>
</asp:Content>

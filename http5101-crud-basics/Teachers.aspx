﻿<%@ Page Title="Teachers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Teachers.aspx.cs" Inherits="http5101_crud_basics.Teachers" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>

    <asp:SqlDataSource runat="server"
        id="teachers_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <!-- The microsoft SQL server
        was linked to this DataGrid instead
    -->
    <div id="teacher_query" class="querybox" runat="server">
    </div>
    <asp:DataGrid id="teachers_list"
        runat="server" >
    </asp:DataGrid>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class Classes : Page
    {
        private string basequery = "SELECT classid, teachers.teacherid, classname as 'Class Name', classcode as Code, convert(varchar, startdate,106) as 'Start Date', convert(varchar, finishdate,106) as 'Finish Date', " +
            " teacherfname+' '+teacherlname as Teacher " +
            "FROM CLASSES join teachers on " +
            "teachers.teacherid = classes.teacherid";

        protected void Page_Load(object sender, EventArgs e)
        {
            classes_select.SelectCommand = basequery;
            //debug.InnerHtml = basequery;
            //classes_list.DataSource = classes_select;
            classes_list.DataSource = Classes_Manual_Bind(classes_select);
            classes_query.InnerHtml = basequery;
            classes_list.DataBind();
        }

        //Need a link to teacher
        //Need a link to class
        protected DataView Classes_Manual_Bind(SqlDataSource src)
        {
            //We should have full control over the ability to pick
            //and choose how we represent our serverside data on the client side HTML

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Class Name"] =
                    "<a href=\"Class.aspx?classid="
                    + row["classid"]
                    + "\">"
                    + row["Class Name"]
                    + "</a>";

                row["Teacher"] =
                    "<a href=\"Teacher.aspx?teacherid="
                    + row["teacherid"]
                    + "\">"
                    + row["Teacher"]
                    + "</a>";



            }
            mytbl.Columns.Remove("teacherid");
            mytbl.Columns.Remove("classid");
            myview = mytbl.DefaultView;

            return myview;

        }
    }
}
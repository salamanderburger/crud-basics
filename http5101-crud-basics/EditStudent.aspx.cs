﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace http5101_crud_basics
{
    public partial class EditStudent : System.Web.UI.Page
    {
        public int studentid
        {
            get { return Convert.ToInt32(Request.QueryString["studentid"]); }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        //This just happens a little bit after Page_Load, same idea.
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView studentrow = getStudentInfo(studentid);
            if (studentrow == null)
            {
                student_name.InnerHtml = "No Student Found.";
                return;
            }
            student_fname.Text = studentrow["studentfname"].ToString();
            student_lname.Text = studentrow["studentlname"].ToString();
            //put this into the title so we know who we're editing
            student_section.SelectedValue = studentrow["studentsection"].ToString();
            if (!Page.IsPostBack)
            {
                student_name.InnerHtml += student_fname.Text + " " + student_lname.Text;
                
            }
            student_number.Text = studentrow["studentnumber"].ToString();

        }

        protected void Edit_Student(object sender, EventArgs e)
        {
            string fname = student_fname.Text;
            string lname = student_lname.Text;
            string section = student_section.SelectedValue.ToString();
            string number = student_number.Text;

            string editquery = "Update students set studentfname='" + fname + "'," +
                "studentlname='" + lname + "',studentsection='" + section + "'," +
                "studentnumber='" + number + "' where studentid="+studentid;
            debug.InnerHtml = editquery;
            edit_student.UpdateCommand = editquery;
            edit_student.Update();
            
        }
        protected DataRowView getStudentInfo(int id)
        {
            string query = "select * from students where studentid="+studentid.ToString();
            student_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the students name outside of
            //the datagrid.
            DataView studentview = (DataView)student_select.Select(DataSourceSelectArguments.Empty);

            //If there is no student in the studentview, an invalid id is passed
            if (studentview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView studentrowview = studentview[0]; //gets the first result which is always the student
            //string studentinfo = studentview[0][colname].ToString();
            return studentrowview;

        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewStudent.aspx.cs" Inherits="http5101_crud_basics.NewStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:SqlDataSource runat="server" id="insert_student"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>

    <h3>New Student</h3>

    <div class="inputrow">
        <label>First Name:</label>
        <asp:textbox id="student_fname" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Last Name:</label>
        <asp:textbox id="student_lname" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Number:</label>
        <asp:textbox id="student_number" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Section:</label>
        <asp:radiobuttonlist id="student_section" runat="server">
            <asp:listitem runat="server" text="A" value="A"></asp:listitem>
            <asp:listitem runat="server" text="B" value="B"></asp:listitem>
        </asp:radiobuttonlist>
    </div>

    <asp:Button Text="Add Student" runat="server" OnClick="AddStudent"/>

    <div class="querybox" id="debug" runat="server">

    </div>

</asp:Content>

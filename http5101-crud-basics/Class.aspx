﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Class.aspx.cs" Inherits="http5101_crud_basics.Class" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="class_name" runat="server"></h3>
    <%-- sql data source for the delete --%>
    <asp:SqlDataSource 
        runat="server"
        id="del_class"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_class_btn"
        OnClick="DelClass"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="del_debug" class="querybox" runat="server"></div>
    <a href="EditClass.aspx?Classid=<%Response.Write(this.classid);%>">Edit</a>

    <%-- data source object which shows base info about the class --%>
   <asp:SqlDataSource runat="server"
        id="class_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- data source object which shows the students in that class --%>
    <asp:SqlDataSource runat="server"
        id="studentxclass_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <%-- container for representing base info --%>

    <div id="class_query" runat="server" class="querybox">
    </div>
    <asp:DataGrid ID="class_list" runat="server">
    </asp:DataGrid>
    <%-- container for representing class info --%>
    <div id="studentxclass_query" runat="server" class="querybox">
    </div>
    <asp:DataGrid ID="studentxclass_list" runat="server">
    </asp:DataGrid>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace http5101_crud_basics
{
    public partial class Students : Page
    {
        private string sqlline = "SELECT studentid, concat(studentfname, ' ', studentlname) as Name, studentnumber as Number, convert(varchar,enrolmentdate,106) as Enrolled, studentsection as Section FROM STUDENTS";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            students_select.SelectCommand = sqlline;
            student_query.InnerHtml = sqlline;
            //students_list.DataSource = students_select;

            students_list.DataSource = Students_Manual_Bind(students_select);

            //To append an extra button we have to
            //disable the auto generate columns
            //bind the SQL result set to our datagrid manually... again

            BoundColumn s_id = new BoundColumn();
            s_id.DataField = "studentid";
            s_id.HeaderText = "ID";
            s_id.Visible = false;
            students_list.Columns.Add(s_id);


            BoundColumn name = new BoundColumn();
            name.DataField = "Name";
            name.HeaderText = "Name";
            students_list.Columns.Add(name);

            BoundColumn number = new BoundColumn();
            number.DataField = "Number";
            number.HeaderText = "Number";
            students_list.Columns.Add(number);

            BoundColumn enrolled = new BoundColumn();
            enrolled.DataField = "Enrolled";
            enrolled.HeaderText = "Enrolled";
            students_list.Columns.Add(enrolled);

            BoundColumn ssection = new BoundColumn();
            ssection.DataField = "Section";
            ssection.HeaderText = "Section";
            students_list.Columns.Add(ssection);

            BoundColumn edit = new BoundColumn();
            edit.DataField = "Edit";
            edit.HeaderText = "Action";
            students_list.Columns.Add(edit);

            ButtonColumn delstudent = new ButtonColumn();
            delstudent.HeaderText = "Delete";
            delstudent.Text = "Delete";
            delstudent.ButtonType = ButtonColumnType.PushButton;
            delstudent.CommandName = "DelStudent";
            students_list.Columns.Add(delstudent);

            students_list.DataBind();
        }

        protected void Students_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName=="DelStudent")
            {
                //int rownum = Convert.ToInt32(e.CommandArgument);
                int studentid = Convert.ToInt32(e.Item.Cells[0].Text);
                //assocstudentid = students_list.R
                DelStudent(studentid);
                //student_query.InnerHtml = "You tried to delete a student! "+studentid;
            }

        }

        protected void DelStudent(int studentid)
        {
            string basequery = "DELETE FROM STUDENTS WHERE STUDENTID=" + studentid.ToString();
            del_student.DeleteCommand = basequery;
            del_student.Delete();

            student_query.InnerHtml = basequery;

            basequery = "DELETE FROM STUDENTSXCLASSES WHERE STUDENTID=" + studentid;
            del_student.DeleteCommand = basequery;
            del_student.Delete();

            student_query.InnerHtml += "But also "+basequery;


        }


        protected DataView Students_Manual_Bind(SqlDataSource src)
        {
            //Don't like the way DataBind() works? Do it yourself!
            //Thisfunction will returna dataview that is substitute for
            //the automatic "dataview" provided by sqldatasource
            DataTable mytbl;

            //Eventually we will bind the data to this variable
            DataView myview;

            //First we need to make a table that pulls from the datasource
            //Sometimes working with a microsoft development environment
            //yields playing silly games with their datatypes and classes
            //to do straightforward transformations
            //ie. instead of DataSource > DataTable
            // microsoft makes us do DataSource > DataView > DataTable
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            //And also "Edit" Link
            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";
            //But now we also want "Delete" Link
            
            mytbl.Columns.Add(editcol);
            //Must add this column onto a datagrid instead.
            //mytbl.Columns.Add(delcol);

            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Name"] = 
                    "<a href=\"Student.aspx?studentid="
                    + row["studentid"]
                    + "\">" 
                    + row["Name"] 
                    + "</a>";

                //Now for every row we specify editcol and delcol too
                //We also can't reference them by their name,
                //both are called "Action"
                row[editcol] = "<a href=\"EditStudent.aspx?studentid=" +
                    row["studentid"]+"\">Edit</a>";

                //Delete is trickier. Create a button which
                //has event "delstudent" that executes on click.
                //Then we need to add that button to the datagrid.
                

                               
            }
            //Going to hide the studentid instead of removing it
            //mytbl.Columns.Remove("studentid");
            myview = mytbl.DefaultView;

            return myview;

        }

        protected void Search_Students(object sender, EventArgs e)
        {
            /*
              This function takes the inputs from the search
              and changes the sqldatasource select command
              to filter down students.
            */
            string newsql = sqlline + " WHERE (1=1) ";
            string key = student_key.Text;
            List<string> sec = new List<string>();
            foreach(ListItem section in student_sec.Items)
            {
                if (section.Selected)
                {
                    sec.Add(section.Value);
                }
            }

            if (key != "")
            {
                newsql +=
                    " AND (CONCAT(STUDENTFNAME, STUDENTLNAME) LIKE '%" + key + "%'" +
                    " OR STUDENTNUMBER LIKE '%" + key + "%') ";
            }
            if (sec.Count>0)
            {
                //This block only executes when there is at least one section
                //selected, meaning the logic won't break if none are.
                newsql += " AND ( ";
                //Adding a condition where students can be in one,
                // 
                foreach (string sectionkey in sec) { 
                    newsql +=
                        " STUDENTSECTION LIKE '%" + sectionkey + "%' OR";
                    //This is extendable to more than two sections.
                    // (
                    // STUDENTSECTION LIKE '%A%' OR
                    // STUDENTSECTION LIKE '%B%' OR
                    // STUDENTSECTION LIKE '%C%' OR
                    // FALSE
                    // )
                }
                newsql += " 1=0)";
                //1=0 means FALSE.
                //( ... ) OR FALSE will only be true  
                //when ( ... ) is true.
            }

            students_select.SelectCommand = newsql;
            student_query.InnerHtml = newsql;

            //Intercept the "auto bind" and create our own binding
            students_list.DataSource = Students_Manual_Bind(students_select);

            students_list.DataBind();

        }
    }
    
}
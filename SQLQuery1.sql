﻿--alter table classes drop index CLASSES_PK;
--alter table students drop index STUDENTS_PK;
--alter table studentsxclasses drop index STUDENTXCLASSES_PK;
--alter table teachers drop index TEACHERS_PK;

alter table classes alter column classid bigint not null;
alter table students alter column studentid bigint not null;
alter table studentsxclasses alter column studentxclassid bigint not null;
alter table teachers alter column teacherid bigint not null;

--alter table classes ADD PRIMARY KEY (classid);
--alter table students ADD PRIMARY KEY (studentid);
--alter table studentsxclasses ADD PRIMARY KEY (studentxclassid);
--alter table teachers ADD PRIMARY KEY (teacherid);


